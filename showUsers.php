<?php

/*
** Author: Jeroen Rijsdijk
** Description: Demo functionality for getting users(max 2) out of a database
*/


if($_SERVER["REQUEST_METHOD"] == "POST")
{
	require 'connection.php';
	showUsers();	
}

function showUsers()
{
	global $connect;
	
	$query = "SELECT * 
				FROM user 
				ORDER BY create_date DESC 
				LIMIT 2";
				
	$result = mysqli_query($connect,$query);
	
	$number_of_rows = mysqli_num_rows($result);
	
	$temp_array = array();
	
	if($number_of_rows > 0)
	{
		while($row = mysqli_fetch_assoc($result))
		{
			$temp_array[] = $row;
		}
	}
	
	header('Content-type: application/json');
	echo json_encode(array("users"=>$temp_array));
	
	mysqli_close($connect);
	
}

?>